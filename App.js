/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import "reflect-metadata"
import React from 'react';
import { WebHelper } from './src/Components/WebHelper'
import { MyContextProvider } from './src/Context/DataContext'
const App = () => {


  return (
    <MyContextProvider>
      <WebHelper />
    </MyContextProvider>
  );
};


export default App;
