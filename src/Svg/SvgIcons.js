import React, { Component } from 'react';
import { View } from 'react-native';
import Svg, { Circle, Ellipse, G, Path } from 'react-native-svg';

const ExitSvg = props => {
  return (
    <Svg height={props.size || 85} width={props.size || 85} viewBox="0 0 91 91">
      <Circle cx={45.5} cy={45.5} r={45.5} fill="none" /* fill={props.color || "#00A99D"} */ />
      <Path d="M36.7 30.5v-7h28.8v44H36.7v-7" fill="none" stroke={props.color || "#E6E6E6"} strokeWidth={6} /> 
      <Path d="M53.756 41.544v8h-35.3v-8z" fill={props.color || "#E6E6E6"} />
      <Path d="M12.5 45.5h17.7v-13zM12.5 45.5h17.7v13z" fill={props.color || "#E6E6E6"} />
    </Svg>
  )
}

const MakePhotoSvg = props => {
  return (
    <Svg height={props.size || 85} width={props.size || 85} viewBox="0 0 96 92">
      <Path d="M21.2 89.5h-8c-5.9 0-10.7-6.3-10.7-14.1V64.8M93.5 64.8v10.6c0 7.8-4.8 14.1-10.7 14.1H73M73 2.5h9.8c5.9 0 10.7 6.3 10.7 14.1v12.9M2.5 29.5V16.6c0-7.8 4.8-14.1 10.7-14.1h8.1"
        fill="none" stroke={props.color || "#E6E6E6"} strokeWidth={5} />
      <Ellipse cx={47.9} cy={45.6} rx={19.9} ry={20.4}
        fill="none" stroke={props.color || "#E6E6E6"} strokeWidth={5} />
    </Svg>
  )
}
const MakePhotoSvg2 = props => {
  return (
    <Svg height={props.size || 85} width={props.size || 85} viewBox="0 0 91.5 91.5">
      <Circle cx={45.8} cy={45.8} r={43.3} fill="none" stroke={props.color || "#E6E6E6"} strokeWidth={5} />
      <Path d="M73.4 28.8l-9.1-2.2-3.6-7.6c-.6-1.6-2-2.7-3.6-2.7H36c-1.7 0-3.2 1.1-3.8 2.7l-3.8 7.6-9.6 2.2c-1.7.4-2.9 2-2.9 3.8V64c0 2 1.5 3.6 3.4 3.6H73c1.8 0 3.3-1.6 3.3-3.6V32.6c-.1-1.8-1.3-3.4-2.9-3.8zM27.5 39.1c-1.7 0-3.1-1.4-3.1-3.1s1.4-3.1 3.1-3.1 3.1 1.4 3.1 3.1-1.4 3.1-3.1 3.1zm18.2 22c-8 0-14.4-6.4-14.4-14.4s6.4-14.4 14.4-14.4 14.4 6.4 14.4 14.4-6.4 14.4-14.4 14.4z"
        fill={props.color || "#E6E6E6"} />
    </Svg>
  )
}

const DeletePhotoSvg = props => {
  return (
    <Svg height={props.size || 85} width={props.size || 85} viewBox="0 0 91 91">
      <Circle cx={45.5} cy={45.5} r={45.5} fill="none"/* fill={props.color || "#B22D36"} */ />
      <Path d="M61.35 20.752l9.899 9.9L31.65 70.247l-9.899-9.9z" fill={props.color || "#B22D36"} />
      <Path d="M70.542 61.066l-9.9 9.9-39.598-39.598 9.9-9.9z" fill={props.color || "#B22D36"} />
    </Svg>
  )
}
const AddPhotoSvg = props => {
  return (
    <Svg height={props.size || 85} width={props.size || 85} viewBox="0 0 91 91">
      <G fill="none">
        <Circle cx={45.5} cy={45.5} r={45.5} fill="none"/* {props.color || "#009245"} */ />
        <Path d="M38.5 17.5h14v56h-14z" fill={props.color || "#009245"} />
        <Path d="M73.5 39.5v14h-56v-14z" fill={props.color || "#009245"} />
      </G>
    </Svg>
  )
}

const ReadySvg = props => {
  return (
    <Svg height={props.size || 85} width={props.size || 85} viewBox="0 0 91 91">
      <Circle cx={45.5} cy={45.5} r={45.5} fill="none"/* {props.color || "#009245"} */ />
      <Path d="M39.6 68.5l-8.7-9.2 36-36.3 9 9z" fill={props.color || "#009245"} />
      <Path d="M48.4 59.8l-8.9 8.7-25-21.9 9.7-9.6z" fill={props.color || "#009245"} />
    </Svg>
  )
}

export {
  ExitSvg,
  MakePhotoSvg,
  // MakePhotoSvg2,
  DeletePhotoSvg,
  AddPhotoSvg,
  ReadySvg,
}
