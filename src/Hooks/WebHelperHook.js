import React from 'react'
import { BackHandler } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'
import { Observations } from '../Entities/Observations'
import { Places } from '../Entities/Places'
import { Forms } from '../Entities/Forms'
import { Status } from '../Entities/Status'
import { Images } from '../Entities/Images'
import { Settings } from '../Entities/Settings'
import { Dictionary } from '../Entities/Dictionary'
import { data_value } from '../Entities/data/data_value'
import { data_value_parameters } from '../Entities/data/data_value_parameters'
import { Reports } from '../Entities/reports/Reports'
import {createConnection} from 'typeorm/browser'
import Orientation from 'react-native-orientation';

function useWebHelper() {
  let webView = React.useRef()
  const [view, setView] = React.useState(1)

  let dbConnection = () => {
    return createConnection({
      type: 'react-native',
      database: 'database',
      synchronize: true,
      location: 'default',
      logging: ['error', 'query', 'schema'],
      entities: [
        Observations,  //Наблюдения
        Places,         //Места
        Forms,          //Формы
        Status,         //Статус отправки
        Images,         //Изображения мест
        Settings,       //Настройки
        Dictionary,    //Словари
        data_value,     //
        data_value_parameters, //
        Reports, //отчеты
        // UniqueData // уникальные коды и их параметры
      ]
    });
  }
  let onAndroidBackPress = () => {

    setView(1)
    return true
  }
  React.useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', onAndroidBackPress);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', onAndroidBackPress);
    };
  }, []);
  React.useEffect(() => {
    AsyncStorage.getItem("alreadyLaunched").then(value =>{
      console.log(value)
      if(value === null){
        AsyncStorage.setItem('alreadyLaunched', true);
      }
      else {

      }
      dbConnection()
    })
  }, [])
 
  let onNavigationStateChange = (nav) => {

  }

  let data = { webView, view }
  let actions = { onNavigationStateChange, setView }

  return {
    data, actions
  }
}

export { useWebHelper }