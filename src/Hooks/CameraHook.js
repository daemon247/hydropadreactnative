import React from 'react'
import { useCamera } from "react-native-camera-hooks";
import { MyContext } from '../Context/DataContext'
import Orientation from 'react-native-orientation'
import {initialState} from '../InitalStates/InitalState'
function useCameraComponent() {
  // const { state, createDispatch } = React.useContext(MyContext)
  const [
    { cameraRef, autoFocus, autoFocusPoint },
    {
      takePicture,
    },
  ] = useCamera(initialState);

  let data = { cameraRef, autoFocus, autoFocusPoint }
  let actions = { takePicture }

  React.useEffect(() => {
    Orientation.lockToPortrait()
    return  () => Orientation.unlockAllOrientations()
  })
  return {
    data, actions
  }

}

export { useCameraComponent }