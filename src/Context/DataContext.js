import React from 'react';

const MyContext = React.createContext()

const initialState = {
    isMainMenu: true,         //Главный экран
    isCreatePhoto: false,     //Отображение сделанной фотографии
    data: {},                 // Данные о фотографии
    isCameraVisible: false,   //Отображение камеры
    isButtonVisible: true,    //отображение кнопки
    counter: null,            //счетчик фотографий
    path: {},                 //Хранение абсолютного пути сделанной фотографии
    height: null,             //Высота картинки
    width: null,              //Ширина картинки
    idObservation: null,      //State для записи id Наблюдения
    currentLongitude: null,   //Долгота
    currentLatitude: null,    //Широта
    resultCodes: {}           //Уникальные коды и их параметры с книжен
}

function MyContextProvider(props) {

    const [state, dispatch] = React.useReducer((state, action) => {
        switch (action.type) {
            //Открытие камеры и запись в state id наблюдения
            case 'Camera':
                return {
                    ...state,
                    isCameraVisible: action.value.isCameraVisible,
                    idObservation: action.value.idObservation
                }

            //Демонстрация сделанной фотографии
            case 'DisplayPhoto':
                return {
                    ...state,
                    isCameraVisible: action.value.false,
                    isButtonVisible: action.value.true
                }
            //Запись количества фотографий в счетчик
            case 'Counter':
                return {
                    ...state,
                    counter: action.value
                }
            //Изменение счетчика на "+" или "-" 1
            case 'SumCounter':
                return {
                    ...state,
                    counter: state.counter + action.value
                }
            //Установка кнопки в положение true или false
            case 'ButtonVisible':
                return {
                    ...state,
                    isButtonVisible: action.value
                }
            case 'isCamera':
                return {
                    ...state,
                    isMainMenu: true,
                    isCreatePhoto: false,
                    isCameraVisible: action.value
                }

            //Запись в state геолокацию пользователя
            case 'Geolocation':
                return {
                    ...state,
                    currentLongitude: action.value.currentLongitude,
                    currentLatitude: action.value.currentLatitude
                }
            // Запись в state данные о фотографии
            case 'TakePicture':
                return {
                    ...state,
                    data: action.value.data,
                    path: action.value.data.uri,
                    width: action.value.data.width,
                    height: action.value.data.height,
                    isCameraVisible: action.value.cameraVisible,
                    isCreatePhoto: action.value.isCreatePhoto
                }
            //Запись уникальных кодов и параметров
            case 'writeCodesResult':

                return {
                    ...state,
                    resultCodes: action.value
                }

            default:
                return state
        }

    }, initialState)

    /** @dispatch -   Изменение состояния state по типу, описанному в case */
    const actions = {
        createDispatch: (type, value) => {
            dispatch({ type: type, value: value })
        },

    }
    // создаем провайдер
    const Provider = MyContext.Provider
    //Запихиваем все что понадобится в других компонентах в value
    let value = { state, dispatch, actions }


    //оборачиваем это ПРовайдером
    return <Provider value={value}>{props.children}</Provider>
}
export {
    MyContextProvider,
    MyContext
}
