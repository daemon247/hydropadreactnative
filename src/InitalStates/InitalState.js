import {  Dimensions } from 'react-native';

//Настройки камеры
export const initialState = {
    flash: 'off', //Вспышка
    zoom: 0, 
    autoFocus: 'on',
    autoFocusPoint: {
        normalized: { x: 0.5, y: 0.5 }, // normalized values required for autoFocusPointOfInterest
        drawRectPosition: {
            x: Dimensions.get('window').width * 0.5 - 32,
            y: Dimensions.get('window').height * 0.5 - 32,
        },
    },
    focusDepth: 0,
    type: 'back', //Фронтальная или задняя камера (front, back)
    whiteBalance: 'auto',
    ratio: '16:9',

    isRecording: false,
    canDetectFaces: false,
    canDetectText: false,
    canDetectBarcode: false,
    faces: [],
    textBlocks: [],
    barcodes: [],
};
//Блокировка зума экрана
export const INJECTEDJAVASCRIPT = 'const meta = document.createElement(\'meta\');' +  
'meta.setAttribute(\'content\', \'width=device-width, initial-scale=1, maximum-scale=0.99, user-scalable=0\');' + 
'meta.setAttribute(\'name\', \'viewport\'); document.getElementsByTagName(\'head\')[0].appendChild(meta); '