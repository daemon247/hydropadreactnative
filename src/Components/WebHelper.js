import React from 'react'
import { View, Button } from 'react-native'
import { WebView } from 'react-native-webview'
import { useWebHelper } from '../Hooks/WebHelperHook'
import { Camera } from './Camera'
import {onMessage} from '../Managers/CaseManager'
function WebHelper() {

  const {
    data: {
      webView, view
    },
    actions: {
      onNavigationStateChange, setView
    }
  } = useWebHelper()


  return <>
  {
    view === 1 && <View style={{ height: '100%' }}>
      <Button title='WebView' onPress={() => setView(2)} /> 
      <Button title='Camera' onPress={() => { setView(3)
       }} />
      {/* <WebView
        nativeConfig={{ props: { webContentsDebuggingEnabled: true } }} //debug for inspect
        ref={webView}
        source={{ uri: 'http://google.com' }}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        onNavigationStateChange={onNavigationStateChange}
      /> */}
    </View>
  }
  {
    view === 2 &&  <WebView
        nativeConfig={{ props: { webContentsDebuggingEnabled: true } }} //debug for inspect
        ref={webView}
        source={{ uri: 'http://192.168.1.112:3000' }}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        onNavigationStateChange={onNavigationStateChange}
        onMessage ={e => console.log(e)}
      /> 
  }
  {
    view === 3 && <Camera />
  }
  </>

}

export { WebHelper }