import React from 'react'
import { RNCamera } from 'react-native-camera'
import { ExitSvg, MakePhotoSvg } from '../Svg/SvgIcons'
import { View, TouchableOpacity, Image } from 'react-native'
import styles from '../Styles/styles'
import { useCameraComponent } from '../Hooks/CameraHook'
function Camera() {
  const {
    data: {
      cameraRef, autoFocus, autoFocusPoint, state
    },
    actions: {
      takePicture, createDispatch
    }
  } = useCameraComponent()
  return <>
    <View style={{ height: "100%", justifyContent: "center" }}>
      <View style={{ flex: 11, justifyContent: "center" }}>
        <RNCamera style={styles.preview} />

        {/* Кнопки Выйти из камеры и Сделать Фото*/}
        <View style={{ flex: 0.3, backgroundColor: "black", flexDirection: "row" }}>

          {/* button-icon exit */}
          <View style={styles.iconContainer}>
            <TouchableOpacity
              //  onPress={() => {}
              // //  backFunc(actions.createDispatch)
              //  } 
              activeOpacity={0.5}
            >
              <ExitSvg />
            </TouchableOpacity>
          </View>

          {/* button-icon makePhoto */}
          <View style={styles.iconContainer}>
            <TouchableOpacity
              // disabled={!state.isButtonVisible}
              // onPress={
              //   // async () => {
              //   //   takePhoto(actions.createDispatch, takePicture, state);
              //   // }
              // }
              activeOpacity={0.5}>
              <MakePhotoSvg size={90} color={
                // !state.isButtonVisible 
                // && 
                "#666"} />
            </TouchableOpacity>
          </View>

          {/* empty */}
          <View style={styles.iconContainer}>
            <TouchableOpacity
              // onPress={() => front_back_func()}
              activeOpacity={0.5}>
              <Image source={require("../Svg/download.png")} style={{ width: 80, height: 80 }} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>

  </>
}

export { Camera }