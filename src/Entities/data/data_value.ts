import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm/browser'
import { data_value_parameters } from './data_value_parameters';

@Entity("data_value")
export class data_value {
    @OneToMany(type => data_value_parameters, dataValueParameters=> dataValueParameters.data_value_id)
    @PrimaryGeneratedColumn()
    id?: number

    @Column('int')
    code?: number

    @Column('varchar')
    datetime_value?:  string


    @Column('varchar')
    value?: string


}