import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, ManyToOne, JoinColumn } from 'typeorm/browser'
import { data_value } from './data_value';

@Entity('data_value_parameters')
export class data_value_parameters {

    @PrimaryGeneratedColumn()
    id?: number

    @Column('varchar')
    parameter_value?: string

    @ManyToOne(type => data_value, dataValue => dataValue.id)
    @JoinColumn({name: 'data_value_id'})
    data_value_id?: data_value[]

    @Column('int')
    parameter_id?: number

    @Column({type: 'boolean', nullable: true })
    changeable?: boolean    
}