import { Entity, PrimaryGeneratedColumn, Column} from 'typeorm/browser'

@Entity('Dictionary')
export class Dictionary {
    @PrimaryGeneratedColumn()
    id?: number

    @Column('varchar')
    dictionary?: string

}