import { Entity, PrimaryGeneratedColumn, Column} from 'typeorm/browser'

@Entity('Reports')
export class Reports {
    @PrimaryGeneratedColumn()
    id?: number

    @Column('varchar')
    title?: string

    @Column('varchar')
    report?: string

    @Column('varchar')
    attribute?: string
}