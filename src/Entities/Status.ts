import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm/browser'
import { Observations } from './Observations';

@Entity('Status')
export class Status {

    @PrimaryGeneratedColumn()
    id?: number

    @Column("varchar")
    name?: string

    @OneToMany(type => Observations, observation => observation.statusId)
    observations?: Observations[]
}