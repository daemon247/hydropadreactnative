
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm/browser'
import { Observations } from './Observations'

@Entity('Forms')
export class Forms {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column("varchar")
    book?: string;

    @OneToMany(type => Observations, observation => observation.formId, {onDelete: 'CASCADE'})
    observations?: Observations[]

    @Column('varchar',{nullable: true})
    pattern?: string;

}
