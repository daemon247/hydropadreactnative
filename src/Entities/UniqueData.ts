import{ Entity, PrimaryGeneratedColumn, Column,  OneToMany } from 'typeorm/browser'

@Entity('UniqueData')
export class UniqueData {

    @PrimaryGeneratedColumn()
    id?:number

    @Column('int')
    code_id?: number

    @Column('varchar')
    parameters?: string

    @Column({type: 'int', nullable: true})
    defaultValue?: number
}