import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn } from 'typeorm/browser'
import { Forms } from './Forms'
import { Places } from './Places'

/* Добавь сюда эти столбцы с начальными данными(!), если можно
 "loginRemember": false,
 "loginUsername": "",
 "loginPassword": ""
 */

@Entity('Settings')
export class Settings {
    @PrimaryGeneratedColumn()

    id?: number

    @Column('int')
    idStation?: number

    @OneToOne(type => Forms)
    @JoinColumn({ name: "formId", referencedColumnName: "id" })
    formId?: Forms[]

    @OneToOne(type => Places)
    @JoinColumn({ name: "placeId", referencedColumnName: "id" })
    placeId?: Places[]

    @Column('int')
    limitOnPage?: number

    @Column({ type: Boolean })
    snowShooting?: null

    @Column({ type: 'varchar' })
    serverIP?: string

    @Column({ type: 'varchar', nullable: true })
    serverPort?: string

    @Column({ type: 'varchar', nullable: true })
    sendingType?: string

    @Column({ type: 'varchar', nullable: true })
    serverFTP?: string

    @Column({ type: 'varchar', nullable: true })
    serverHTTP?: string

    @Column({ type: 'varchar', nullable: true })
    serverSMTP?: string

    @Column({ type: 'int', nullable: true })
    portFTP?: number

    @Column({ type: 'varchar', nullable: true })
    portHTTP?: string

    @Column({ type: 'varchar', nullable: true })
    portSMTP?: string

    @Column({ type: 'varchar', nullable: true })
    userFTP?: string

    @Column({ type: 'varchar', nullable: true })
    passwordFTP?: string

    @Column({ type: 'varchar', nullable: true })
    userSMTP?: string

    @Column({ type: 'varchar', nullable: true })
    passwordSMTP?: string

    @Column({ type: 'varchar', nullable: true })
    mailSenderSMTP?: string

    @Column({ type: 'varchar', nullable: true })
    mailRecipientSMTP?: string

}