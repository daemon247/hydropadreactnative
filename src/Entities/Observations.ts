import { Entity, PrimaryGeneratedColumn, Column,  ManyToOne,  JoinColumn,  } from 'typeorm'
import { Forms } from './Forms'
import { Places } from './Places'
import { Status } from './Status'

@Entity('Observations')
export class Observations {

    @PrimaryGeneratedColumn()
    id?: number

    @Column('int')
    idStation?: number

    @Column({ type: 'varchar' })
    date?: string

    @Column({ type: 'varchar' })
    dateReal?: string

    @Column({type: 'varchar', nullable: true})
    xml?: string
    
    @ManyToOne(type => Places, place => place.observations)
    @JoinColumn({ name: 'placeId' })
    placeId?: Places[]

    @ManyToOne(type => Forms, form => form.observations)
    @JoinColumn({ name: 'formId' })
    formId?: Forms[]

    @ManyToOne(type => Status, status => status.observations)
    @JoinColumn({ name: 'statusId' })
    statusId?: Status[];

    @Column({type:'varchar', nullable: true})
    errorType?: string
    
}