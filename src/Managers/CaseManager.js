






















function onMessage(event, webView){
    const { data } = event.nativeEvent;

    console.log(data)

}


// export {onMessage}
// import { showCameraView, isVisible } from './CameraManager'
// import * as manager from './dbManager'
// import { xmlCreate, senderXml } from './DataXML'
// import * as restman from './REST_Manager'
// // import filterdata_value_parameters from './dataValuesManager'
// import filterdata_value_parameters from './newReportresult'
// import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
// import { Alert } from 'react-native'
// import readOnly from './readOnlyInputs';
/** Подключение JSON файлов
 */

/**
 * Слушатель событий, который присылает клиент
 * @param {any} event 
 * @param {any} webView 
 * @param {any} createDispatch 
 * @param {any} state 
 */
// let onMessage = (event, webView, createDispatch, state) => {


//     let injectWebView = (emitter, payload) => {
//         let script = `window.emitter.emit('${emitter}','${JSON.stringify(payload)}')`
//         webView.injectJavaScript(script)
//     }
//     /** данные, которые приходят со стороны клиента */
//     const { data } = event.nativeEvent;
//     /** Парс данных */
//     let input = JSON.parse(data);
//     /** switch для сообщений с клиента */
//     console.log('_____________get case')
//     switch (input.type) {
//         /* __________________new Cases новая технолоджи_______________________ */
//         case 'CreateTable':
//             manager.create("Places",
//                 [
//                     { name: "Речной гидрологический пост (ГП)", shortName: "Река" },
//                     { name: "Озерной гидрологический пост (ГП)", shortName: "Озеро" }
//                 ])

//             manager.create("Forms", [
//                 { book: "RU-1", pattern: null, version: 0.3 },
//                 { book: "RU-2", pattern: null, version: 0.3 },
//                 { book: "RU-3", pattern: null, version: 0.3 }
//             ])
//             manager.create("Status", [
//                 { name: "Черновик" },
//                 { name: "Ошибка" },
//                 { name: "Отправлено" },
//                 { name: "Отправка.." }
//             ])
//             break
//         case 'ReadBasic':
//             console.log('_________________________ReadBasic')
//             Promise.all([
//                 manager.read("Status"),
//                 manager.read("Forms"),
//                 manager.read("Places"),
//                 // restman.createCodeIdParameters(createDispatch),
//             ]).then(result => {
//                 console.log('_________________________ReadBasic2', result)

//                 let payload = {
//                     status: result[0],
//                     forms: result[1],
//                     places: result[2],
//                     // codes: result[3].codes,
//                     // interval: result[3].proc_t,
//                 }
//                 injectWebView('read', payload)
//             })
//             break;
//         case 'ReadSettings':
//             manager.read(input.table[0].toUpperCase() + input.table.slice(1))
//                 .then(result => {
//                     let payload = {
//                         [input.table]: result[0][0],
//                     }
//                     injectWebView('read', payload)
//                 })
//             break
//         case 'ReadTable':
//             /** делаем 1 букву заглавной, т.к. input.table все буквы маленькие */
//             // manager.read(input.table[0].toUpperCase() + input.table.slice(1))
//             manager.read(input.table)
//                 .then(result => {
//                     console.log('__________________ReadTable______________', result)
//                     // injectWebView('read', result)
//                     let payload = {
//                         [input.table]: result,
//                     }
//                     injectWebView('read', result)
//                 })
//             break;
//         case 'ReadTableWithId':
//             manager.readID(input.table, input.id).then(result => {
//                 let payload = {
//                     [input.table]: result[0],
//                 }
//                 injectWebView('read', payload)
//             })
//             break;
//         /* ___________________________________________________________________ */

//         /** Тестовое внесение данных в таблицу */
//         case 'CreateObservation':
//             Promise.all([
//                 manager.readElement('Observations', { dateFormat: input.dateFormat, formId: input.formId }), //проверка по дате и книжки на наличие наблюдения и его data_values в БД 
//                 readOnly(input.dateFormat, input.formId, input.copyPattern) // устанавливает ридонли в полях, которые уже есть на это время и дату в заполненых полях других книжек
//             ]).then(res => {
//                 // console.log('res[0].length === ', res[0].length)
//                 if (res[0].length === 0) { // срабатывает, когда нет такого наблюдения по выбранной дате и книжки
//                     manager.create("Observations", {
//                         idStation: input.idStation,
//                         dateFormat: input.dateFormat,
//                         date: input.date,
//                         time: input.time,
//                         realDate: input.realDate,
//                         xml: null,
//                         placeId: input.placeId,
//                         formId: input.formId,
//                         statusId: input.statusId,
//                         copyPattern: encodeURI(res[1]),
//                         versionPattern: input.versionPattern,
//                     }).then((table) => {
//                         let script = `window.emitter.emit('OpenIdObservation', ${JSON.stringify({ id: table.identifiers[0].id, copyPattern: res[1] })})`
//                         webView.injectJavaScript(script)
//                         createDispatch('Counter', 0)
//                     })
//                 }
//                 //id в res нет и не может быть, потому что id я не посылаю вообще, его генерит БД
//                 else { //срабатывает, когда наблюдение уже есть
//                     // console.log(res[0][0].id)
//                     let script = `window.emitter.emit('OpenIdObservation', ${JSON.stringify({ id: res[0][0].id })})`
//                     webView.injectJavaScript(script)
//                 }
//             })
//             break;
//         case 'CheckFirstRun':
//             manager.read('Settings').then(result => {
//                 if (!result[0]) {
//                     let script = `window.emitter.emit('read','${JSON.stringify({ openSettings: true })}')`
//                     webView.injectJavaScript(script)
//                 }
//                 else {
//                     let script = `window.emitter.emit('read','${JSON.stringify({ openSettings: false })}')`
//                     webView.injectJavaScript(script)
//                 }
//             })
//             break;
//         // Вход в приложение
//         case 'ReadAll':
//             /** RNRestart.Restart() */
//             Promise.all([
//                 manager.read("Observations"),
//                 manager.read("Settings"),
//                 manager.read("Status"),
//                 manager.read("Forms"),
//                 manager.read("Places"),
//                 restman.createCodeIdParameters(createDispatch),
//             ]).then(result => {

//                 // console.log(result[9])
//                 let payload = {
//                     observations: result[0],
//                     settings: result[1][0],
//                     status: result[2],
//                     forms: result[3],
//                     places: result[4],
//                     codes: result[5].codes,
//                     interval: result[5].proc_t,
//                 }
//                 injectWebView('read', payload)
//             })
//             break;
//         //Чтение любой таблицы с БД, также чтение по id с БД
//         case 'ReadTable2':
//             if (input.id) {
//                 manager.readID(input.table[0].toUpperCase() + input.table.slice(1), input.id).then(result => {
//                     let myResult = input.table === 'settings' ? result[0][0] : result[0];
//                     let payload = {
//                         [input.table]: myResult,
//                     }
//                     injectWebView('read', payload)
//                 })
//             }
//             else {
//                 Promise.all([
//                     manager.read(input.table[0].toUpperCase() + input.table.slice(1)), /** делаем 1 букву заглавной, т.к. input.table все буквы маленькие */
//                 ]).then(result => {
//                     let myResult = input.table === 'settings' ? result[0][0] : result[0];
//                     let payload = {
//                         [input.table]: myResult,
//                     }
//                     injectWebView('read', payload)
//                 })
//             }
//             break;
//         case 'ReadImageForID':
//             manager.readImg(input.id_obs).then(async result => {

//                 // let script = `window.emitter.emit("readImgID", ${JSON.stringify(result)})`
//                 /** Запись в счетчик количество фотографий */
//                 createDispatch("Counter", result.length)
//                 injectWebView('readImgID', result)
//                 /** Отправка данных */
//                 // webView.injectJavaScript(script)
//             })
//             break;
//     }
    
// }

export { onMessage }

